#include <stdio.h>


#include "mem_internals.h"
#include "mem.h"
#include <assert.h>

#define HEAP_SIZE 1024

//Обычное успешное выделение памяти.
void test_successful_allocation() {
    printf("Test: Successful Allocation\n");
    void* ptr = _malloc(32);
    assert(ptr != NULL && "Allocation failed");
    debug_heap(stdout, HEAP_START);
    _free(ptr);
    debug_heap(stdout, HEAP_START);
    printf("Test passed!\n\n");
}

// Освобождение одного блока из нескольких выделенных.
void test_free_one_block() {
    printf("Test: Free One Block\n");
    void* ptr1 = _malloc(20);
    void* ptr2 = _malloc(40);
    void* ptr3 = _malloc(60);
    debug_heap(stdout, HEAP_START);
    _free(ptr2);
    debug_heap(stdout, HEAP_START);
    _free(ptr1);
    debug_heap(stdout, HEAP_START);
    _free(ptr3);
    debug_heap(stdout, HEAP_START);
    printf("Test passed!\n\n");
}

// Освобождение двух блоков из нескольких выделенных.
void test_free_two_blocks() {
    printf("Test: Free Two Blocks\n");
    void* ptr1 = _malloc(20);
    void* ptr2 = _malloc(40);
    void* ptr3 = _malloc(60);
    debug_heap(stdout, HEAP_START);
    _free(ptr2);
    _free(ptr1);
    debug_heap(stdout, HEAP_START);
    _free(ptr3);
    debug_heap(stdout, HEAP_START);
    printf("Test passed!\n\n");
}

// Память закончилась, новый регион памяти расширяет старый
void test_memory_extension() {
    printf("Test: Memory Extension\n");
    void* ptr1 = _malloc(20);
    void* ptr2 = _malloc(40);
    void* ptr3 = _malloc(60);
    debug_heap(stdout, HEAP_START);
    _free(ptr2);
    _free(ptr1);
    debug_heap(stdout, HEAP_START);
    void* ptr4 = _malloc(80);
    debug_heap(stdout, HEAP_START);
    _free(ptr3);
    _free(ptr4);
    debug_heap(stdout, HEAP_START);
    printf("Test passed!\n\n");
}

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
// новый регион выделяется в другом месте.
void test_new_region_allocation() {
    printf("Test: New Region Allocation\n");
    void* ptr1 = _malloc(20);
    void* ptr2 = _malloc(40);
    debug_heap(stdout, HEAP_START);
    _free(ptr2);
    _free(ptr1);
    debug_heap(stdout, HEAP_START);
    debug_heap(stdout, HEAP_START);
    printf("Test passed!\n\n");
}


int main() {
    heap_init(HEAP_SIZE);

    test_successful_allocation();
    test_free_one_block();
    test_free_two_blocks();
    test_memory_extension();
    test_new_region_allocation();

    heap_term();

    return 0;
}
